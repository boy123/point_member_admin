<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hadiah extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Hadiah_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'hadiah/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'hadiah/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'hadiah/index.html';
            $config['first_url'] = base_url() . 'hadiah/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Hadiah_model->total_rows($q);
        $hadiah = $this->Hadiah_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'hadiah_data' => $hadiah,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'hadiah/hadiah_list',
            'konten' => 'hadiah/hadiah_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Hadiah_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_hadiah' => $row->id_hadiah,
		'image' => $row->image,
		'potongan_point' => $row->potongan_point,
	    );
            $this->load->view('hadiah/hadiah_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hadiah'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'hadiah/hadiah_form',
            'konten' => 'hadiah/hadiah_form',
            'button' => 'Create',
            'action' => site_url('hadiah/create_action'),
	    'id_hadiah' => set_value('id_hadiah'),
        'image' => set_value('image'),
	    'label' => set_value('label'),
	    'potongan_point' => set_value('potongan_point'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $img = upload_gambar_biasa('hadiah', 'image/hadiah/', 'jpg|png|jpeg|gif', 10000, 'image');
            $data = array(
        'image' => $img,
		'label' => $this->input->post('label',TRUE),
		'potongan_point' => $this->input->post('potongan_point',TRUE),
	    );

            $this->Hadiah_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('hadiah'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Hadiah_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'hadiah/hadiah_form',
                'konten' => 'hadiah/hadiah_form',
                'button' => 'Update',
                'action' => site_url('hadiah/update_action'),
		'id_hadiah' => set_value('id_hadiah', $row->id_hadiah),
        'image' => set_value('image', $row->image),
		'label' => set_value('label', $row->label),
		'potongan_point' => set_value('potongan_point', $row->potongan_point),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hadiah'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_hadiah', TRUE));
        } else {
            $data = array(
		'image' => $retVal = ($_FILES['image']['name'] == '') ? $_POST['image_old'] : upload_gambar_biasa('hadiah', 'image/hadiah/', 'jpeg|png|jpg|gif', 10000, 'image'),
        'potongan_point' => $this->input->post('potongan_point',TRUE),
		'label' => $this->input->post('label',TRUE),
	    );

            $this->Hadiah_model->update($this->input->post('id_hadiah', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('hadiah'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Hadiah_model->get_by_id($id);

        if ($row) {
            $this->Hadiah_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('hadiah'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('hadiah'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('potongan_point', 'potongan point', 'trim|required');

	$this->form_validation->set_rules('id_hadiah', 'id_hadiah', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Hadiah.php */
/* Location: ./application/controllers/Hadiah.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-09-15 13:13:03 */
/* https://jualkoding.com */