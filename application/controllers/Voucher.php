<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Voucher extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Voucher_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'voucher/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'voucher/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'voucher/index.html';
            $config['first_url'] = base_url() . 'voucher/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Voucher_model->total_rows($q);
        $voucher = $this->Voucher_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'voucher_data' => $voucher,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'voucher/voucher_list',
            'konten' => 'voucher/voucher_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Voucher_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_voucher' => $row->id_voucher,
		'image' => $row->image,
	    );
            $this->load->view('voucher/voucher_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('voucher'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'voucher/voucher_form',
            'konten' => 'voucher/voucher_form',
            'button' => 'Create',
            'action' => site_url('voucher/create_action'),
	    'id_voucher' => set_value('id_voucher'),
	    'image' => set_value('image'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $img = upload_gambar_biasa('voucher', 'image/voucher/', 'jpg|png|jpeg|gif', 10000, 'image');
            $data = array(
		'image' => $img,
	    );

            $this->Voucher_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('voucher'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Voucher_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'voucher/voucher_form',
                'konten' => 'voucher/voucher_form',
                'button' => 'Update',
                'action' => site_url('voucher/update_action'),
		'id_voucher' => set_value('id_voucher', $row->id_voucher),
		'image' => set_value('image', $row->image),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('voucher'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_voucher', TRUE));
        } else {
            $data = array(
		'image' => $retVal = ($_FILES['image']['name'] == '') ? $_POST['image_old'] : upload_gambar_biasa('voucher', 'image/voucher/', 'jpeg|png|jpg|gif', 10000, 'image'),
	    );

            $this->Voucher_model->update($this->input->post('id_voucher', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('voucher'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Voucher_model->get_by_id($id);

        if ($row) {
            $this->Voucher_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('voucher'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('voucher'));
        }
    }

    public function _rules() 
    {
	// $this->form_validation->set_rules('image', 'image', 'trim|required');

	$this->form_validation->set_rules('id_voucher', 'id_voucher', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Voucher.php */
/* Location: ./application/controllers/Voucher.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-09-15 13:14:07 */
/* https://jualkoding.com */