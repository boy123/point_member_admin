<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {


    public function log_user()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'konten' => 'a_user/log_user',
            'judul_page' => 'Log User',
        );
        $this->load->view('v_index', $data);
    }
	
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function point_log()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'konten' => 'point/view',
            'judul_page' => 'History Point',
        );
        $this->load->view('v_index', $data);
    }

    public function detail_hadiah($id_hadiah)
    {
        $data = array(
            'konten' => 'hadiah/detail_hadiah',
            'judul_page' => 'Detail Hadiah',
        );
        $this->load->view('v_index', $data);
    }

    public function detail_voucher($id_voucher)
    {
        $data = array(
            'konten' => 'voucher/detail_voucher',
            'judul_page' => 'Kirim Voucher',
        );
        $this->load->view('v_index', $data);
    }

    public function detail_undian($id_undian)
    {
        $data = array(
            'konten' => 'undian/detail_undian',
            'judul_page' => 'Kirim undian',
        );
        $this->load->view('v_index', $data);
    }

    public function tambah_point($id_user)
    {
        $point = $this->input->post('point');
        $tambah = tambahPoint($id_user,$point, '', '');
        if ($tambah) {
            $this->session->set_flashdata('message', alert_biasa('Point berhasil ditambahkan','success'));
                redirect('users','refresh');
        }
    }

    public function konfirmasi_voucher($id_user,$id_voucher)
    {
        $this->db->where('id_user', $id_user);
        $this->db->where('id_voucher', $id_voucher);
        $update = $this->db->update('list_voucher', array('dikonfirmasi'=>'1','date_konfirmasi'=>get_waktu()));
        if ($update) {
            $this->session->set_flashdata('message', alert_biasa('Voucher berhasil dikonfimasi !','success'));
                redirect('app/detail_voucher/'.$id_voucher,'refresh');
        }
    }

    public function konfirmasi_hadiah($id_user,$id_hadiah)
    {
        $this->db->where('id_user', $id_user);
        $this->db->where('id_hadiah', $id_hadiah);
        $update = $this->db->update('list_hadiah', array('dikonfirmasi'=>'1','date_konfirmasi'=>get_waktu()));
        if ($update) {
            $this->session->set_flashdata('message', alert_biasa('Hadiah berhasil dikonfimasi !','success'));
                redirect('app/detail_hadiah/'.$id_hadiah,'refresh');
        }
    }
   
	
}
