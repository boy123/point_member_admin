<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Undian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Undian_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'undian/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'undian/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'undian/index.html';
            $config['first_url'] = base_url() . 'undian/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Undian_model->total_rows($q);
        $undian = $this->Undian_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'undian_data' => $undian,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'undian/undian_list',
            'konten' => 'undian/undian_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Undian_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_undian' => $row->id_undian,
		'image' => $row->image,
		'judul_undian' => $row->judul_undian,
		'mulai' => $row->mulai,
		'selesai' => $row->selesai,
	    );
            $this->load->view('undian/undian_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('undian'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'undian/undian_form',
            'konten' => 'undian/undian_form',
            'button' => 'Create',
            'action' => site_url('undian/create_action'),
	    'id_undian' => set_value('id_undian'),
	    'image' => set_value('image'),
	    'judul_undian' => set_value('judul_undian'),
	    'mulai' => set_value('mulai'),
        'selesai' => set_value('selesai'),
        'potongan_point' => set_value('potongan_point'),
	    'status' => set_value('status'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
             $img = upload_gambar_biasa('user', 'image/undian/', 'jpg|png|jpeg|gif', 10000, 'image');
            $data = array(
		'image' => $img,
		'judul_undian' => $this->input->post('judul_undian',TRUE),
		'mulai' => $this->input->post('mulai',TRUE),
        'selesai' => $this->input->post('selesai',TRUE),
        'potongan_point' => $this->input->post('potongan_point',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Undian_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('undian'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Undian_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'undian/undian_form',
                'konten' => 'undian/undian_form',
                'button' => 'Update',
                'action' => site_url('undian/update_action'),
		'id_undian' => set_value('id_undian', $row->id_undian),
		'image' => set_value('image', $row->image),
		'judul_undian' => set_value('judul_undian', $row->judul_undian),
		'mulai' => set_value('mulai', $row->mulai),
        'selesai' => set_value('selesai', $row->selesai),
        'potongan_point' => set_value('potongan_point', $row->potongan_point),
		'status' => set_value('status', $row->status),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('undian'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_undian', TRUE));
        } else {
            $data = array(
		'image' => $retVal = ($_FILES['image']['name'] == '') ? $_POST['image_old'] : upload_gambar_biasa('user', 'image/undian/', 'jpeg|png|jpg|gif', 10000, 'image'),
		'judul_undian' => $this->input->post('judul_undian',TRUE),
		'mulai' => $this->input->post('mulai',TRUE),
        'selesai' => $this->input->post('selesai',TRUE),
        'potongan_point' => $this->input->post('potongan_point',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Undian_model->update($this->input->post('id_undian', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('undian'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Undian_model->get_by_id($id);

        if ($row) {
            $this->Undian_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('undian'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('undian'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('judul_undian', 'judul undian', 'trim|required');
	$this->form_validation->set_rules('mulai', 'mulai', 'trim|required');
	$this->form_validation->set_rules('selesai', 'selesai', 'trim|required');

	$this->form_validation->set_rules('id_undian', 'id_undian', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Undian.php */
/* Location: ./application/controllers/Undian.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-09-15 13:15:46 */
/* https://jualkoding.com */