<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_point_member extends CI_Controller {

	public function tes_fcm()
	{
		$server_key = get_setting('server_fcm_customer');
		$token = get_data('users','id_user',"46",'token_fcm');
		$title = "Tes Aja";
		$body = "Hai Ini Tes";
		$screen ="voucher";
		$this->send_notif($server_key,$token,$title, $body, $screen);
	}

	public function kirim_voucher()
	{
		$id_voucher = $this->input->get('id_voucher');
		$kirim_banyak = $this->input->get('kirim_banyak');
		if ($kirim_banyak == '1') {
			$user = $this->db->get_where('users', array('level'=>'user'));
			foreach ($user->result() as $rw) {
				$simpan = $this->db->insert('list_voucher', array(
					'id_voucher'=>$id_voucher,
					'id_user'=>$rw->id_user,
					'date_at'=>get_waktu()
				));
				if ($simpan) {
					$server_key = get_setting('server_fcm_customer');
					$token = get_data('users','id_user',$rw->id_user,'token_fcm');
					$title = "Selamat Kamu mendapatkan voucher belaja";
					$body = "Hai $rw->nama_lengkap, kamu mendapatkan voucher tukarkan sekarang juga";
					$screen ="voucher";
					$this->send_notif($server_key,$token,$title, $body, $screen);
				}
				$this->session->set_flashdata('message', alert_biasa('Voucher berhasil dikirim','success'));
				redirect('voucher','refresh');
			}
		} else {
			$id_user = $this->input->get('id_user');
			$nama = get_data('users','id_user',$id_user,'nama_lengkap');
			$simpan = $this->db->insert('list_voucher', array(
					'id_voucher'=>$id_voucher,
					'id_user'=>$id_user,
					'date_at'=>get_waktu()
				));
				if ($simpan) {
					$server_key = get_setting('server_fcm_customer');
					$token = get_data('users','id_user',$id_user,'token_fcm');
					$title = "Selamat Kamu mendapatkan voucher belaja";
					$body = "Hai $nama_lengkap, kamu mendapatkan voucher tukarkan sekarang juga";
					$screen ="voucher";
					$this->send_notif($server_key,$token,$title, $body, $screen);
				}
				$this->session->set_flashdata('message', alert_biasa('Voucher berhasil dikirim','success'));
				redirect('voucher','refresh');
		}
		
	}

	public function kirim_pemenang()
	{
		$id_list_undian = $this->input->get('id_list_undian');
		$id_user = $this->input->post('id_user');
		$this->db->where('id_list_undian', $id_list_undian);
		$simpan = $this->db->update('list_undian', array('pemenang'=>'1'));
		if ($simpan) {
			$server_key = get_setting('server_fcm_customer');
			$token = get_data('users','id_user',$id_user,'token_fcm');
			$title = "Selamat Kamu memenangkan Undian";
			$body = "Hai , kamu telah memenangkan undian klaim sekarang juga";
			$screen ="undian";
			$this->send_notif($server_key,$token,$title, $body, $screen);
		}
		$this->session->set_flashdata('message', alert_biasa('Voucher berhasil dikirim','success'));
		redirect('undian','refresh');
		
		
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	

	public function daftar()
	{
		if ($_POST) {
			$nama = $this->input->post('nama');
			$no_telp = $this->input->post('no_telp');
			$password = $this->input->post('password');
			$email = $this->input->post('email');

			$cek = $this->db->get_where('users', array('no_telp'=>$no_telp));
			if ($cek->num_rows() > 0) {
				$result = array(
					'status' => "0",
					'pesan' => 'No Telp Sudah terdaftar, silahkan login !'
				);
				echo json_encode($result);
			} else {
				$simpan = $this->db->insert('users', array(
					'nama_lengkap' => $nama,
					'username' => $no_telp,
					'password' => $password,
					'no_telp' => $no_telp,
					'email' => $email,
					'level' => 'user'
				));

				if ($simpan) {
					$result = array(
						'status' => "1",
						'pesan' => 'Pendaftaran berhasil, silahkan login'
					);
					echo json_encode($result);
				} else {
					$result = array(
						'status' => "0",
						'pesan' => 'Gagal'
					);
					echo json_encode($result);
				}
			}

			
		}
	}

	

	public function login()
	{
		if ($_POST) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$token_fcm = $this->input->post('token_fcm');
			$level = $this->input->post('level');

			$cek = $this->db->get_where('users', array('username' => $username, 'password' => $password,'level'=>$level));

			if ($cek->num_rows() == 1) {
				$data = $cek->row();

				// update fcm token
				$this->db->where('id_user', $data->id_user);
				$this->db->update('users', array('token_fcm'=>$token_fcm));
				$result = array(
					'status' => "1",
					'id_user' => $data->id_user,
					'nama' => $data->nama_lengkap,
					'no_telp' => $data->no_telp,
					'password' => $data->password,
					'email' => $data->email,
					'level' => $data->level,
					'pesan' => "Selamat datang dan selamat beraktifitas $data->nama_lengkap"
				);
				echo json_encode($result);
			} else {
				$result = array(
					'status' => "0",
					'pesan' => 'Gagal'
				);
				echo json_encode($result);
			}
		}
	}

	public function get_point()
	{
		$req = $this->input->post('request');
		$id_user = $this->input->post('id_user');
		if ($req != '') {
			echo json_encode(array('total_point'=>getPoint($id_user)));
		}
	}

	public function get_promotion()
	{
		$req = $this->input->post('request');
		$data = $this->db->get_where('promotion',array('status'=>'1'));
		$result = array();

		foreach ($data->result() as $rw) {
			
			array_push($result, array(
				'image' => $rw->image,
			));
		}

		echo json_encode(array(
			'detailPromo' => $result
		));
	}


	public function get_slider()
	{
		$req = $this->input->post('request');
		$this->db->order_by('id_slide', 'desc');
		$data = $this->db->get('slider');
		$result = array();

		foreach ($data->result() as $rw) {
			
			array_push($result, array(
				'image' => $rw->image,
			));
		}

		echo json_encode(array(
			'detailSlider' => $result
		));
	}


	public function get_undian()
	{
		$req = $this->input->post('request');
		$this->db->order_by('id_undian', 'desc');
		$data = $this->db->get('undian');
		$result = array();

		foreach ($data->result() as $rw) {

			$total_kupon = $this->db->get_where('list_undian', array('id_undian'=>$rw->id_undian))->num_rows();
			if ($total_kupon == '') {
				$total_kupon = '0';
			}
			
			array_push($result, array(
				'id_undian' => $rw->id_undian,
				'image' => $rw->image,
				'judul' => $rw->judul_undian,
				'mulai' => $rw->mulai,
				'selesai' => $rw->selesai,
				'status' => $rw->status,
				'potongan_point'=> $rw->potongan_point,
				'total_kupon'=>$total_kupon
			));
		}

		echo json_encode(array(
			'detailnya' => $result,
		));
	}

	public function get_history_point()
	{
		$req = $this->input->post('request');
		$id_user = $this->input->post('id_user');
		$this->db->where('id_user', $id_user);
		$this->db->order_by('id_history_point', 'desc');
		$data = $this->db->get('history_point');
		$result = array();

		foreach ($data->result() as $rw) {
			array_push($result, array(
				'in_point' => $rw->in_point,
				'out_point' => $rw->out_point,
				'date_at' => $rw->date_at,
				'status' => $rw->status,
			));
		}

		echo json_encode(array(
			'detailnya' => $result,
		));
	}

	public function get_hadiah()
	{
		$req = $this->input->post('request');
		$id_user = $this->input->post('id_user');
		$this->db->order_by('id_hadiah', 'desc');
		$data = $this->db->get('hadiah');
		$result = array();

		foreach ($data->result() as $rw) {

			$cek_hadiah = $this->db->get_where('list_hadiah', array('id_hadiah'=>$rw->id_hadiah,'id_user'=>$id_user))->num_rows();
			if ($cek_hadiah == '') {
				$ambil = '0';
			} else {
				$ambil = '1';
			}
			
			array_push($result, array(
				'id_hadiah' => $rw->id_hadiah,
				'image' => $rw->image,
				'potongan_point'=> $rw->potongan_point,
				'label'=> $rw->label,
				'ambil'=>$ambil
			));
		}

		echo json_encode(array(
			'detailnya' => $result,
		));
	}

	public function get_voucher()
	{
		$req = $this->input->post('request');
		$id_user = $this->input->post('id_user');
		$this->db->order_by('id_voucher', 'desc');
		$data = $this->db->get_where('list_voucher',array('id_user'=>$id_user));
		$result = array();

		if ($data->num_rows() > 0) {
			foreach ($data->result() as $rw) {
			
				array_push($result, array(
					'id_voucher' => $rw->id_voucher,
					'image' => get_data('voucher','id_voucher',$rw->id_voucher,'image'),
					'diambil' => $rw->diambil
				));
			}

			echo json_encode(array(
				'status' => '1',
				'detailnya' => $result,
			));
		} else {
			echo json_encode(array(
				'status' => '0'
			));
		}

		
	}

	public function ambilHadiah()
	{
		if ($_POST) {
			$id_user = $this->input->post('id_user');
			$id_hadiah = $this->input->post('id_hadiah');
			$point = $this->input->post('point');

			//cek point cukup
			if (getPoint($id_user) > $point) {
				$data = array(
					'id_user' => $id_user,
					'id_hadiah' => $id_hadiah,
					'point' => $point,
					'diambil' => '1',
					'date_at' => get_waktu(),
					'date_ambil' => get_waktu()
				); 
				$this->db->insert('list_hadiah', $data);
				$id = $this->db->insert_id();
				$point_now = kurangiPoint($id_user,$point, 'hadiah', $id_hadiah);
				if ($point_now) {
					$result = array(
						'status' => "1",
						'pesan' => 'Hadiah sudah bisa di ambil dikasir'
					);
					echo json_encode($result);
				}

			} else {
				$result = array(
					'status' => "0",
					'pesan' => 'Maaf Point Anda tidak Cukup'
				);
				echo json_encode($result);
			}

			
		}


	}

	public function ambilVoucher()
	{
		if ($_POST) {
			$id_user = $this->input->post('id_user');
			$id_voucher = $this->input->post('id_voucher');

			$this->db->where('id_user', $id_user);
			$this->db->where('id_voucher', $id_voucher);
			$data = array(
				'diambil' => '1',
				'date_ambil' => get_waktu()
			); 
			$update = $this->db->update('list_voucher', $data);
			//cek point cukup
			if ($update) {
				
				$result = array(
					'status' => "1",
					'pesan' => 'Voucher sudah bisa di ambil dikasir'
				);
				echo json_encode($result);

			} else {
				$result = array(
					'status' => "0",
					'pesan' => 'Maaf Ada kesalahan'
				);
				echo json_encode($result);
			}

			
		}


	}

	public function ikutiUndian()
	{
		if ($_POST) {
			$id_user = $this->input->post('id_user');
			$id_undian = $this->input->post('id_undian');
			$point = $this->input->post('point');

			//cek point cukup
			if (getPoint($id_user) > $point) {
				$data = array(
					'id_user' => $id_user,
					'id_undian' => $id_undian,
					'point' => $point,
					'date_at' => get_waktu()
				); 
				$this->db->insert('list_undian', $data);
				$id = $this->db->insert_id();
				$point_now = kurangiPoint($id_user,$point, 'undian', $id_undian);
				$this->db->where('id_list_undian', $id);
				$simpan = $this->db->update('list_undian', array('no_undian'=>'UND'.$id));
				if ($simpan) {
					$result = array(
						'status' => "1",
						'pesan' => 'Undian ini berhasil anda ikuti'
					);
					echo json_encode($result);
				}

			} else {
				$result = array(
					'status' => "0",
					'pesan' => 'Maaf Point Anda tidak Cukup'
				);
				echo json_encode($result);
			}

			
		}


	}

	public function get_no_undian()
	{
		$id_user = $this->input->post('id_user');
		$this->db->select('lu.no_undian, un.judul_undian, un.status');
		$this->db->from('list_undian lu');
		$this->db->join('undian un', 'un.id_undian = lu.id_undian', 'inner');
		$this->db->where('un.status', '1');
		$this->db->where('lu.id_user', $id_user);
		$this->db->order_by('lu.id_list_undian', 'desc');
		$data = $this->db->get();
		$result = array();

		foreach ($data->result() as $rw) {
			
			array_push($result, array(
				'no_undian' => $rw->no_undian,
				'judul' => $rw->judul_undian,
				'status' => $retVal = ($rw->status == '1') ? 'Berlangsung' : 'Selesai',
			));
		}

		echo json_encode(array(
			'detailnya' => $result,
		));
	}

	

	public function update_profil()
	{
		if ($_POST) {
			$id_user = $this->input->post('id_user');
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$no_telp = $this->input->post('no_telp');
			$password = $this->input->post('password');

			$data = array();

			if ($password != '') {
				$data = array(
					'nama_lengkap' => $nama,
					'no_telp' => $no_telp,
					'email' => $email,
					'password' => $password
				);
			} else {
				$data = array(
					'nama_lengkap' => $nama,
					'no_telp' => $no_telp,
					'email' => $email,
				);
			}

			$this->db->where('id_user', $id_user);
			$update = $this->db->update('users', $data);
			if ($update) {
				echo json_encode(array(
					'status' => '1',
					'pesan' => 'Data berhasil di ubah',
				));
			}


		}
	}

	
	private function send_notif($server_key,$token,$title, $body, $screen)
	{
		# agar diparse sebagai JSON di browser
		header('Content-Type:application/json');

		# atur zona waktu sender server ke Jakarta (WIB / GMT+7)
		date_default_timezone_set("Asia/Jakarta");


		$headers = [
		'Content-Type:application/json',
		'Accept:application/json',
		'Authorization: key='.$server_key.''
		];


		// echo $post_raw_json;
		// exit();
		

		# Inisiasi CURL request
		$ch = curl_init();

		# atur CURL Options
		curl_setopt_array($ch, array(
		CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send', # URL endpoint
		CURLOPT_HTTPHEADER => $headers, # HTTP Headers
		CURLOPT_RETURNTRANSFER => 1, # return hasil curl_exec ke variabel, tidak langsung dicetak
		CURLOPT_FOLLOWLOCATION => 1, # atur flag followlocation untuk mengikuti bila ada url redirect di server penerima tetap difollow
		CURLOPT_CONNECTTIMEOUT => 60, # set connection timeout ke 60 detik, untuk mencegah request gantung saat server mati
		CURLOPT_TIMEOUT => 60, # set timeout ke 120 detik, untuk mencegah request gantung saat server hang
		CURLOPT_POST => 1, # set method request menjadi POST
		CURLOPT_POSTFIELDS => '{"notification": {"body": "'.$body.'","title": "'.$title.'","sound": "default","badge":"1"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "screen": "'.$screen.'", "status": "done", "body" : "'.$body.'"}, "to": "'.$token.'"}', # attached post data dalam bentuk JSON String,
		// CURLOPT_VERBOSE => 1, # mode debug
		// CURLOPT_HEADER => 1, # cetak header
		CURLOPT_SSL_VERIFYPEER => true  
		));

		# eksekusi CURL request dan tampung hasil responsenya ke variabel $resp
		$resp = curl_exec($ch);

		# validasi curl request tidak error
		if (curl_errno($ch) == false) {
		# jika curl berhasil
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($http_code == 200) {
		  # http code === 200 berarti request sukses (harap pastikan server penerima mengirimkan http_code 200 jika berhasil)
		  // echo $resp;
			$send = '{"notification": {"body": "'.$body.'","title": "'.$title.'","sound": "default","badge":"1"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "screen": "'.$screen.'", "status": "done", "body" : "'.$body.'"}, "to": "'.$token.'"}';
			$this->db->insert('log_notif', array('log'=>$send,'resp'=>$resp));
		} else {
		  # selain itu request gagal (contoh: error 404 page not found)
		  // echo 'Error HTTP Code : '.$http_code."\n";
		  // echo $resp;
			$send = '{"notification": {"body": "'.$body.'","title": "'.$title.'","sound": "default","badge":"1"}, "priority": "high", "data": {"click_action": "FLUTTER_NOTIFICATION_CLICK", "screen": "'.$screen.'", "status": "done", "body" : "'.$body.'"}, "to": "'.$token.'"}';
			$this->db->insert('log_notif', array('log'=>$send,'resp'=>$resp));
		}
		} else {
		# jika curl error (contoh: request timeout)
		# Daftar kode error : https://curl.haxx.se/libcurl/c/libcurl-errors.html
		// echo "Error while sending request, reason:".curl_error($ch);
		}

		# tutup CURL
		curl_close($ch);
	}





}
