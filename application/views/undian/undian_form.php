
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Image </label>
            <input type="file" class="form-control" name="image" id="image" placeholder="Image" value="<?php echo $image; ?>" />
            <input type="hidden" name="image_old" value="<?php echo $image ?>">
            <div>
                <?php if ($image != ''): ?>
                    <b>*) Gambar Sebelumnya : </b><br>
                    <img src="image/undian/<?php echo $image ?>" style="width: 100px;">
                <?php endif ?>
            </div>
        </div>
	    <div class="form-group">
            <label for="varchar">Judul Undian <?php echo form_error('judul_undian') ?></label>
            <input type="text" class="form-control" name="judul_undian" id="judul_undian" placeholder="Judul Undian" value="<?php echo $judul_undian; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Mulai <?php echo form_error('mulai') ?></label>
            <input type="date" class="form-control" name="mulai" id="mulai" placeholder="Mulai" value="<?php echo $mulai; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Selesai <?php echo form_error('selesai') ?></label>
            <input type="date" class="form-control" name="selesai" id="selesai" placeholder="Selesai" value="<?php echo $selesai; ?>" />
        </div>
        <div class="form-group">
            <label for="int">Potongan Point <?php echo form_error('potongan_point') ?></label>
            <input type="text" class="form-control" name="potongan_point" id="potongan_point" placeholder="Potongan Point" value="<?php echo $potongan_point; ?>" />
        </div>

        <div class="form-group">
            <label for="int">Status</label>
            <select name="status" class="form-control">
                <option value="<?php echo $status ?>"><?php echo $retVal = ($status == '1') ? 'Sedang berlangsung' : 'Selesai' ; ?></option>
                <option value="1">Sedang berlangsung</option>
                <option value="0">Selesai</option>
            </select>
        </div>
	    <input type="hidden" name="id_undian" value="<?php echo $id_undian; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('undian') ?>" class="btn btn-default">Cancel</a>
	</form>
   