<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="example1">
			<thead>
				<tr>
					<th>No.</th>
					<th>No Undian</th>
					<th>No Telp</th>
					<th>Nama User</th>
					<th>Date At</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				foreach ($this->db->get_where('list_undian', array('id_undian'=>$this->uri->segment(3)))->result() as $rw) {
				 ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $rw->no_undian ?></td>
					<td><?php echo get_data('users','id_user',$rw->id_user,'no_telp') ?></td>
					<td><?php echo get_data('users','id_user',$rw->id_user,'nama_lengkap') ?></td>
					<td><?php echo $rw->date_at ?></td>
					<td>
						<a onclick="javasciprt: return confirm('Are You Sure ?')" href="Api_point_member/kirim_pemenang?id_list_undian=<?php echo $rw->id_list_undian ?>&id_user=<?php echo $rw->id_user ?>&kirim_banyak=0" class="label label-success">Pilih Jadi Pemenang</a>
					</td>
				</tr>
				<?php $no++; } ?>
			</tbody>
		</table>
	</div>
</div>