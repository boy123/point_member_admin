<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="example1">
			<thead>
				<tr>
					<th>No.</th>
					<th>No Telp</th>
					<th>Nama User</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				foreach ($this->db->get_where('list_hadiah', array('id_hadiah'=>$this->uri->segment(3)))->result() as $rw) {
				 ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo get_data('users','id_user',$rw->id_user,'no_telp'); ?></td>
					<td><?php echo get_data('users','id_user',$rw->id_user,'nama_lengkap'); ?></td>
					<td>
						<?php if ($rw->diambil == '1'): ?>
							<span class="label label-info">Sudah diambil</span>
							<?php if ($rw->dikonfirmasi == '' or $rw->dikonfirmasi == '0'): ?>
								<a onclick="javasciprt: return confirm('Are You Sure ?')" href="app/konfirmasi_hadiah/<?php echo $rw->id_user ?>/<?php echo $this->uri->segment(3) ?>" class="label label-primary">Konfirmasi</a>
							<?php else: ?>
							<span class="label label-success">Selesai</span>
							<?php endif ?>
						<?php endif ?>
						
					</td>
				</tr>
				<?php $no++; } ?>
			</tbody>
		</table>
	</div>
</div>