
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Image </label>
            <input type="file" class="form-control" name="image" id="image" placeholder="Image" value="<?php echo $image; ?>" />
            <input type="hidden" name="image_old" value="<?php echo $image ?>">
            <div>
                <?php if ($image != ''): ?>
                    <b>*) Gambar Sebelumnya : </b><br>
                    <img src="image/hadiah/<?php echo $image ?>" style="width: 100px;">
                <?php endif ?>
            </div>
        </div>
        <div class="form-group">
            <label for="int">Label </label>
            <input type="text" class="form-control" name="label" id="label" placeholder="Label hadiah" value="<?php echo $label; ?>" required/>
        </div>
	    <div class="form-group">
            <label for="int">Potongan Point <?php echo form_error('potongan_point') ?></label>
            <input type="text" class="form-control" name="potongan_point" id="potongan_point" placeholder="Potongan Point" value="<?php echo $potongan_point; ?>" />
        </div>
	    <input type="hidden" name="id_hadiah" value="<?php echo $id_hadiah; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('hadiah') ?>" class="btn btn-default">Cancel</a>
	</form>
   