
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('a_user/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <!-- <div class="col-md-3 text-right">
                <form action="<?php echo site_url('a_user/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('a_user'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div> -->
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px" id="example1">
            <thead>
            <tr>
                <th>No</th>
        <th>Nama Lengkap</th>
        <th>Username</th>
        <th>Level</th>
        <th>Foto</th>
      
        <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $no = 1;
            $a_user_data = $this->db->get_where('users', array('level'=>'admin'));
            foreach ($a_user_data->result() as $a_user)
            {
                ?>
                <tr>
            <td width="80px"><?php echo $no ?></td>
            <td><?php echo $a_user->nama_lengkap ?></td>
            <td><?php echo $a_user->username ?></td>
            <td><?php echo $retVal = ($a_user->level == 'user') ? 'member' : 'admin' ; ?></td>
            <td><img src="image/user/<?php echo $a_user->foto ?>" style="width: 100px;"></td>
            <td style="text-align:center" width="200px">
                <?php 
                echo anchor(site_url('a_user/update/'.$a_user->id_user),'<span class="label label-info">Ubah</span>'); 
                echo ' | '; 
                echo anchor(site_url('a_user/delete/'.$a_user->id_user),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                ?>
            </td>
        </tr>
                <?php
                $no++;
            }
            ?>
            </tbody>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!-- <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a> -->
        </div>
            <div class="col-md-6 text-right">
                <?php //echo $pagination ?>
            </div>
        </div>
    