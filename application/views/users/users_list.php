
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('users/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('users/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('users'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Nama Lengkap</th>
		<th>Username</th>
		<th>Password</th>
		<th>No Telp</th>
		<th>Email</th>
		<th>Foto</th>
        <th>Level</th>
		<th>Total Point</th>
		<th>Action</th>
            </tr><?php
            $no = 1;
            $users_data = $this->db->get_where('users', array('level'=>'user'));
            foreach ($users_data->result() as $users)
            {
                ?>
                <tr>
			<td width="80px"><?php echo $no ?></td>
			<td><?php echo $users->nama_lengkap ?></td>
			<td><?php echo $users->username ?></td>
			<td><?php echo $users->password ?></td>
			<td><?php echo $users->no_telp ?></td>
			<td><?php echo $users->email ?></td>
			<td><img src="image/user/<?php echo $users->foto ?>" style="width: 100px;"></td>
            <td>member</td>
			<td><?php echo get_data('point','id_user',$users->id_user,'point') ?></td>
			<td style="text-align:center" width="200px">

                <a class="label label-success" data-toggle="modal" data-target="#<?php echo $users->id_user ?>myModal">Tambah Point</a>

                <!-- Modal -->
                  <div class="modal fade" id="<?php echo $users->id_user ?>myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                    
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Tambah Point</h4>
                        </div>
                        <div class="modal-body">
                        <form action="app/tambah_point/<?php echo $users->id_user ?>" method="post">
                            
                            <div class="form-group">
                                <label>Point yang ditambah</label>
                                <input type="text" name="point" class="form-control">
                            </div>
                           <!--  <div class="form-group">
                                <label>Keterangan</label>
                                <textarea class="form-control" rows="3" name="ket"></textarea>
                            </div> -->
                        
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Simpan</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                        </form>
                      </div>
                      
                    </div>
                  </div>

				<?php 
				echo anchor(site_url('users/update/'.$users->id_user),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('users/delete/'.$users->id_user),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
                $no++;
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    