
<div class="row">
	
	<div class="col-md-12">
		<div class="table-responsive">
		<table class="table table-bordered" id="example1">
			<thead>
				<tr>
					<td>No</td>
					<td>Keterangan</td>
					<td>Date</td>
					<td>User</td>
					<td>In Point</td>
					<td>Out Point</td>
				</tr>
			</thead>
			<tbody>
			<?php 
			$total_masuk = 0;
			$total_keluar = 0;
			$no = 1;
			$this->db->order_by('id_history_point', 'desc');
			$data = $this->db->get('history_point');
			foreach ($data->result() as $rw) {
				// log_r($rw);

			if ($rw->status != '') {
				// log_r($rw->status);
				if ($rw->status == 'undian') {
					$dt = $this->db->get_where('undian', array('id_undian'=>$rw->id_nya));
					if ($dt->num_rows() > 0) {
						$judul = $dt->row()->judul_undian;
					}
				} elseif ($rw->status == 'hadiah') {
					$judul = "Hadiah";
				}
			}

			 ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td>
					<?php 
						echo $retVal = ($rw->status == '') ? 'Point Masuk' : $judul. ' ('.$rw->status.')';
						 ?>
					</td>
					<td><?php echo $rw->date_at ?></td>
					<td><?php echo get_data('users','id_user',$rw->id_user,'nama_lengkap') ?></td>
					<td><?php echo $rw->in_point; $total_masuk = $total_masuk + $rw->in_point;  ?></td>
					<td><?php echo $rw->out_point;  $total_keluar = $total_keluar + $rw->out_point; ?></td>
				</tr>
			<?php $no++; } ?>
			
			</tbody>
		</table>
		</div>
		<br>
		<div class="alert alert-info">
			Total Masuk : <?php echo $total_masuk ?>
		</div>
		<div class="alert alert-success">
			Total Keluar : <?php echo $total_keluar ?>
		</div>
	</div>
</div>