
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Image <?php echo form_error('image') ?></label>
            <input type="file" class="form-control" name="image" id="image" placeholder="Image" value="<?php echo $image; ?>" />
            <div>
                <?php if ($image != ''): ?>
                    <b>*) Gambar Sebelumnya : </b><br>
                    <img src="image/voucher/<?php echo $image ?>" style="width: 100px;">
                <?php endif ?>
            </div>
        </div>
	    <input type="hidden" name="id_voucher" value="<?php echo $id_voucher; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('voucher') ?>" class="btn btn-default">Cancel</a>
	</form>
   