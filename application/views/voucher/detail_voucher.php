<div class="row">
	<div class="col-md-12">
		<p>
			<a href="Api_point_member/kirim_voucher?id_voucher=<?php echo $this->uri->segment(3) ?>&kirim_banyak=1" class="btn btn-info">Kirim ke semua</a>
		</p>
		<table class="table table-bordered" id="example1">
			<thead>
				<tr>
					<th>No.</th>
					<th>No Telp</th>
					<th>Nama User</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				foreach ($this->db->get_where('users', array('level'=>'user'))->result() as $rw) {
					$list_voucher = $this->db->get_where('list_voucher', array('id_voucher'=>$this->uri->segment(3),'id_user'=>$rw->id_user));
				 ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $rw->no_telp; ?></td>
					<td><?php echo $rw->nama_lengkap; ?></td>
					<td>
						<?php if ($list_voucher->num_rows() > 0): 
							$list_voucher = $list_voucher->row();
							?>

							<?php if ($list_voucher->diambil == '1'): ?>
								<span class="label label-info">Sudah diambil</span>
								<?php if ($list_voucher->dikonfirmasi == ''): ?>
									<a onclick="javasciprt: return confirm('Are You Sure ?')" href="app/konfirmasi_voucher/<?php echo $rw->id_user ?>/<?php echo $this->uri->segment(3) ?>" class="label label-primary">Konfirmasi</a>
								<?php else: ?>
								<span class="label label-success">Selesai</span>
								<?php endif ?>
							<?php else: ?>
								<span class="label label-info">Sudah dikirim, namun belum diambil</span>
							<?php endif ?>
						<?php else: ?>
							<a onclick="javasciprt: return confirm('Are You Sure ?')" href="Api_point_member/kirim_voucher?id_voucher=<?php echo $this->uri->segment(3) ?>&id_user=<?php echo $rw->id_user ?>&kirim_banyak=0" class="label label-warning">Kirim</a>
						<?php endif ?>
						
						
					</td>
				</tr>
				<?php $no++; } ?>
			</tbody>
		</table>
	</div>
</div>